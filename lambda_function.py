import json
import requests
import boto3
from botocore.exceptions import ClientError

ses = boto3.client('ses', region_name="us-east-1")


def sendMail(message):
    try:
        response = ses.send_email(
            Source = "debbymags@gmail.com",
            Destination={
                'ToAddresses': [
                    "debbymags@gmail.com"
                ]
            },
            Message={
                'Subject': {
                    'Data': "test"
                },
                'Body': {
                    'Text': {
                        'Data': message
                    }
                }
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])

def getRequest():
    try:
        result = requests.get("https://qk5o0o7w6i.execute-api.us-east-2.amazonaws.com/dev/log")
        res = result.json()
    except requests.exceptions.RequestException as e:
        print(e.response['Error']['Message'])
    else:
        print("API call success")
        #print(result.json())

    return json.dumps(res)

def lambda_handler(event, context):
    # TODO implement
    sendMail(getRequest())
    
    return {
        'statusCode': 200,
        'body': json.dumps('Done running!')
    }
